import React, { Component } from "react";

export default class HandleConditionalRender extends Component {
  state = {
    currentState: false
  };

  checkCondition = () => {
    var randomNum = Math.floor(Math.random() * 10) + 1;
    console.log(randomNum);
    var condition = false;
    if (randomNum > 5) {
      condition = true;
    }
    this.setState({
      currentState: condition
    });
  };

  render() {
    console.log("render");
    return (
      <div>
        <button onClick={this.checkCondition}>check conditional render</button>
        <br />
        {this.state.currentState && (
          <div>condition is true, thats why this part is rendered</div>
        )}
        <button onClick={this.props.returnMainPage}>Back to main Page</button>
      </div>
    );
  }
}
