import React, { Component } from "react";

export default class Forms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valueInput: "",
      valueTextrea: "",
      valueSelect: "coconut",
      selectedFile: null,
      java: false,
      cpp: false,
      numberOfGuests: 2,
      multipleInputForm: false
    };
    this.handleMultipleInputChange = this.handleMultipleInputChange.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleTextareaChange = this.handleTextareaChange.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleFile = e => {
    this.setState({ selectedFile: e.target.files[0], loaded: 0 }, () => {
      const fileElem = this.inputElm.files[0];
      let file = fileElem;
      console.log(file.name);
    });
  };

  handleInputChange = event => {
    this.setState({ valueInput: event.target.value });
  };

  handleTextareaChange = event => {
    this.setState({ valueTextrea: event.target.value });
  };

  handleSelectChange = event => {
    this.setState({ valueSelect: event.target.value });
  };

  handleMultiInput = () =>{
    console.log(this.state.java)
    console.log(this.state.cpp)
    console.log(this.state.numberOfGuests)
    
  }

  handleSubmit(event) {
    //alert('A name was submitted: ' + this.state.value);
    console.log("The input name was submitted: " + this.state.valueInput);
    console.log("The Textarea was submitted: " + this.state.valueTextrea);
    console.log("The Select value was submitted: " + this.state.valueSelect);
    event.preventDefault();
  }

  handleMultipleInputChange(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    console.log("handleMultipleInputChange: " + value);
    this.setState({
      [name]: value
    });

    if(name === "java"){
      this.setState({
        java: value
      })
    }

    if(name === "cpp"){
      this.setState({
        cpp: value
      })
    }
  }
  handleMultipleInputForm = () => {
    this.setState({
      multipleInputForm: !this.state.multipleInputForm
    });
  };

  render() {
    const multipleInputFormJsx = (
      <form>
        <label>
          Java:
          <input
            name="java"
            type="checkbox"
            checked={this.state.java}
            onChange={this.handleMultipleInputChange}
          />
        </label>
        <br />
        <label>
          C++:
          <input
            name="cpp"
            type="checkbox"
            checked={this.state.cpp}
            onChange={this.handleMultipleInputChange}
          />
        </label>
        <br />
        <label>
          Number of guests:
          <input
            name="numberOfGuests"
            type="number"
            value={this.state.numberOfGuests}
            onChange={this.handleMultipleInputChange}
          />
        </label>
      </form>
    );

    const formJsx = (
      <form onSubmit={this.handleSubmit}>
        <label>
          Name:
          <input
            type="text"
            value={this.state.value}
            onChange={this.handleInputChange}
          />
        </label>
        <br />
        <br />
        <label>
          Essay:
          <textarea
            value={this.state.value}
            onChange={this.handleTextareaChange}
          />
        </label>
        <br />
        <label>
          Pick your favorite flavor:
          <select
            value={this.state.valueSelect}
            onChange={this.handleSelectChange}
          >
            <option value="grapefruit">Grapefruit</option>
            <option value="lime">Lime</option>
            <option value="coconut">Coconut</option>
            <option value="mango">Mango</option>
          </select>
        </label>
        <br />
        {
          <input
            className="small-margin"
            id="file-up"
            type="file"
            onChange={this.handleFile}
            ref={inpElm => (this.inputElm = inpElm)}
          />
        }
        <br />
        <input type="submit" value="Submit" />
      </form>
    );

    return (
      <div>
        {formJsx}
        <br />
        <button onClick={this.handleMultipleInputForm}>Multiple Input</button>
        <br />
        {this.state.multipleInputForm && multipleInputFormJsx}
        {this.state.multipleInputForm && (
          <button onClick={this.handleMultiInput}>Console</button>
        )}
        <br />
        <button onClick={this.props.returnMainPage}>Back to main Page</button>
      </div>
    );
  }
}
