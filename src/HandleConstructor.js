import React, { Component } from "react";

export default class HandleConostructor extends Component {
  constructor(props) {
    super(props);
    this.state = {consData: "This data is setted by constructor"};
  }

  
  render() {
    return (
      <div>
        <div>{this.state.consData}</div>
        <button onClick={this.props.returnMainPage}>
          Back to main Page
        </button>
      </div>
    );
  }
}
