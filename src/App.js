import React, { Component } from "react";
import "./App.css";
import HandleState from "./HandleState"
import HandleProps from "./HandleProps"
import HandleConstructor from "./HandleConstructor"
import HandleConditionalRender from "./HandleConditionalRender"
import HandleLifeCycle from "./HandleLifeCycle"
import HandleEvents from "./HandleEvents"
import HandleKeyAndList from "./HandleKeyAndList"
import Forms from "./Forms"
import TemperatureCalculator from "./TemperatureCalculator"
import HandleComposition from "./HandleComposition"

export default class App extends Component {

state = {
  buttonClicked: "",
  heading: "React"
}

  handleState = () => {
    this.setState({
      buttonClicked: "handleState",
      heading: "State"
    })
  }

  handleProps = () => {
    this.setState({
      buttonClicked: "handleProps",
      heading: "Props"
    })
  }


  handleConstructor = () => {
    this.setState({
      buttonClicked: "handleConstructor",
      heading: "Constructor"
    })
  }

  handleConditionalRender = () => {
    this.setState({
      buttonClicked: "handleConditionalRender",
      heading: "Conditional Render"
    })
  }


  handleLifeCycle = () =>{
    this.setState({
      buttonClicked: "reactLifeCycle",
      heading: "Life Cycle"
    })
  }
handleEvents = () =>{
  this.setState({
    buttonClicked: "handleEvents",
    heading: "Handle Events"
  })
}

handleKeyandList = () =>{
  this.setState({
    buttonClicked: "handleKeyandList",
    heading: "Handle Key and List"
  })
}

handleForm = () =>{
  this.setState({
    buttonClicked: "handleForm",
    heading: "Handle Forms"
  })
}

  returnMainPage = () => {
    this.setState({
      buttonClicked: "",
      heading: "React"
    })
  }

  handleTemperatureCalculator = () => {
    this.setState({
      buttonClicked: "handleTemperatureCalculator",
      heading: "Temperature Calculator"
    })
  }

  handleComposition= () => {
    this.setState({
      buttonClicked: "handleComposition",
      heading: "Composition"
    })
  }


  render() {
    
    const buttonJsx = 
    (<div className="App">
    <h1>{this.state.heading}</h1>
    <button onClick={this.handleState}>State</button><br/>
    <button onClick={this.handleProps}>Props</button><br/>
    <button onClick={this.handleConstructor}>Constructor</button><br/>
    <button onClick={this.handleConditionalRender}>Conditional rendering</button><br/>
    <button onClick={this.handleLifeCycle}>LifeCycle</button><br/>
    <button onClick={this.handleEvents}>Events</button><br/>
    <button onClick={this.handleKeyandList}>Key and List</button><br/>
    <button onClick={this.handleForm}>Forms</button><br/>
    <button onClick={this.handleTemperatureCalculator}>Temperature Calculator</button><br/>
    <button onClick={this.handleComposition}>Composition</button><br/>
  </div>);

    var buttonClicked = this.state.buttonClicked;
    if (buttonClicked === ""){
      return (
        <div>{buttonJsx}</div>
      );
    }else if (buttonClicked === "handleState"){
      return (
        <div className="App">
          <h1>{this.state.heading}</h1>
          <HandleState returnMainPage={this.returnMainPage}/>
        </div>
      );
    }else if (buttonClicked === "handleProps"){
      return (
        <div className="App">
          <h1>{this.state.heading}</h1>
          <HandleProps returnMainPage={this.returnMainPage}
          // you can not pass a function which return somethings 
          // and display from receiver class as props
          propsFunc={"props function executes from HandleProps class"}/>
        </div>
      );
    }else if (buttonClicked === "handleConstructor"){
      return (
        <div className="App">
          <h1>{this.state.heading}</h1>
          <HandleConstructor returnMainPage={this.returnMainPage}/>
        </div>
      );
    }else if (buttonClicked === "handleConditionalRender"){
      return (
        <div className="App">
          <h1>{this.state.heading}</h1>
          <HandleConditionalRender returnMainPage={this.returnMainPage}/>
        </div>
      );
    }else if (buttonClicked === "reactLifeCycle"){
      return (
        <div className="App">
          <h1>{this.state.heading}</h1>
          <HandleLifeCycle returnMainPage={this.returnMainPage}/>
        </div>
      );
    }else if (buttonClicked === "handleEvents"){
      return (
        <div className="App">
          <h1>{this.state.heading}</h1>
          <HandleEvents returnMainPage={this.returnMainPage}/>
        </div>
      );
    }else if (buttonClicked === "handleKeyandList"){
      return (
        <div className="App">
          <h1>{this.state.heading}</h1>
          <HandleKeyAndList returnMainPage={this.returnMainPage}/>
        </div>
      );
    }else if (buttonClicked === "handleForm"){
      return (
        <div className="App">
          <h1>{this.state.heading}</h1>
          <Forms returnMainPage={this.returnMainPage}/>
        </div>
      );
    }else if (buttonClicked === "handleTemperatureCalculator"){
      return (
        <div className="App">
          <h1>{this.state.heading}</h1>
          <TemperatureCalculator returnMainPage={this.returnMainPage}/>
        </div>
      );
    }else if (buttonClicked === "handleComposition"){
      return (
        <div className="App">
          <h1>{this.state.heading}</h1>
          <HandleComposition returnMainPage={this.returnMainPage}/>
        </div>
      );
    }
    
  }
}
