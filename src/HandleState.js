import React, { Component } from "react";

export default class HandleState extends Component {

  state = {
    currentState: 1
  }

  handleChangeState = () => {
    var randomNum = Math.floor(Math.random() * 10) + 1;
   // console.log(randomNum);
    this.setState({
      currentState: randomNum
    })
  }

  render() {
    return (
      <div>
       
        <div>State: {this.state.currentState}</div>
        <button onClick={this.handleChangeState}>
          Change State
        </button><br/>
        {this.state.currentState === 10 && <div>
          State: {this.state.currentState} is the top state.
          </div>}<br/>
        <button onClick={this.props.returnMainPage}>
          Back to main Page
        </button>
      </div>
    );
  }
}
