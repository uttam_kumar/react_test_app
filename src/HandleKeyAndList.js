import React, { Component } from "react";

export default class HandleKeyAndList extends Component {
  state = {
    handleNumberListFunctionClicked: false,
    handleNestedRenderFunctionClicked: false,
    handleUniqueKeyRenderFunctionClicked: false
  };

  handleListKey = numbers => {
    //const numbers = [1, 2, 3, 4, 5];
    const doubled = numbers.map(number => number * 2);
    console.log("total elements are: " + doubled);
    console.log("First element is: " + doubled[0]);
  };

  handleNumberListFunction = () => {
    this.setState({
      handleNumberListFunctionClicked: !this.state
        .handleNumberListFunctionClicked
    });
  };

  handleUniqueKeyRenderFunction = () => {
    this.setState({
      handleUniqueKeyRenderFunctionClicked: !this.state
        .handleUniqueKeyRenderFunctionClicked
    });
  };
  handleListKeyWithoutPram = () => {
    const numbers = [1, 2, 3, 4, 5];
    const doubled = numbers.map(number => "number: " + number);
    console.log("total elements are: " + doubled);
    console.log("First element is: " + doubled[0]);
  };

  handleNestedRenderFunction = () => {
    this.setState({
      handleNestedRenderFunctionClicked: !this.state
        .handleNestedRenderFunctionClicked
    });
  };

  render() {
    const numbers = [1, 2, 3, 4, 5];
    const listItems = numbers.map(number => (
      <li key={number.toString()}>{number}</li>
    ));

    function NumberList(props) {
      const numbers = props.numbers;
      const listItems = numbers.map(number => (
        <li key={number.toString()}>{number * 2}</li>
      ));
      return <ul>{listItems}</ul>;
    }

    function ListItem(props) {
      // Correct! There is no need to specify the key here:
      return <li>{props.value}</li>;
    }

    function NumberListKey(props) {
      const numbers = props.numbers;
      const listItems = numbers.map(number => (
        // Correct! Key should be specified inside the array.
        <ListItem key={number.toString()} value={number} />
      ));
      return <ul>{listItems}</ul>;
    }

    function Blog(props) {
        const sidebar = (
          <ul>
            {props.posts.map((post) =>
              <li key={post.id}>
                {post.title}
              </li>
            )}
          </ul>
        );
        const content = props.posts.map((post) =>
          <div key={post.id}>
            <h3>{post.title}</h3>
            <p>{post.content}</p>
          </div>
        );
        return (
          <div>
            {sidebar}
            <hr />
            {content}
          </div>
        );
      }
      
      const posts = [
        {id: 1, title: 'Hello World', content: 'Welcome to learning React!'},
        {id: 2, title: 'Installation', content: 'You can install React from npm.'}
      ];

    return (
      <div>
        <button onClick={this.handleNumberListFunction}>
          Executing render's function
        </button>
        <br />
        {this.state.handleNumberListFunctionClicked && (
          <div>
            <NumberList numbers={numbers} />
          </div>
        )}
        <button onClick={e => this.handleListKey(numbers)}>
          List Key with pram
        </button>
        <br />
        <button onClick={this.handleListKeyWithoutPram}>
          List key without pram
        </button>
        <br />
        <div>{listItems}</div>

        <button onClick={this.handleNestedRenderFunction}>
          Handle Nested Render's Function
        </button>
        {this.state.handleNestedRenderFunctionClicked && (
          <div>
            <NumberListKey numbers={numbers} />
          </div>
        )}
        <br />
        <button onClick={this.handleUniqueKeyRenderFunction}>
          Key Must Be Unique
        </button><br/>
        {this.state.handleUniqueKeyRenderFunctionClicked && (
          <div>
             <Blog posts={posts} />
          </div>
        )}
        <button onClick={this.props.returnMainPage}>Back to main Page</button>
      </div>
    );
  }
}
