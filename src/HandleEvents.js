import React, { Component } from "react";

export default class HandleEvents extends Component {
    constructor(props) {
        super(props);
        this.state = {toggleData: "toggle_1"};
        this.handleTClick = this.handleTClick.bind(this);
      }

    state = {
        eventName: "no event"
    }

    onClickEvent = () => {
        this.setState({
            eventName: "onClick event"
        })
    }

    toggle = () =>{
        this.setState({
            toggleData : this.state.toggleData === "toggle_1" ? "toggle_2" : "toggle_1"
        })
    }
    handleTClick() {
        this.setState(state => ({
          isToggleOn: !state.isToggleOn
        }));
      }

      handleClickWithPram =(e, name)=>{
        //console.log("this is: " + this);
          console.log("this is: " + e);
          console.log("this is ss : " + name);
      }

  render() {
    return (
      <div>
          <div>{this.state.eventName}</div>
        <button onClick={this.onClickEvent}>
          onClick
        </button><br/>
        <button onClick={this.toggle}>
          {this.state.toggleData}
        </button><br/>
        <button onClick={this.handleTClick}>
        {this.state.isToggleOn ? 'True' : 'False'}
        </button><br/>

        <button onClick={(e) => this.handleClickWithPram(e, "something")}>
        onClick with pram
        </button><br/>
        <button onClick={this.props.returnMainPage}>
          Back to main Page
        </button>
      </div>
    );
  }
}
