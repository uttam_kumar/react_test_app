import React, { Component } from "react";

export default class TemperatureCalculator extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      temperature: 0,
      selectScale: "C",
      convertedTemperature: "32 F"
    };

    this.handleSelectScale = this.handleSelectScale.bind(this);
  }

  handleSelectScale = event => {
    this.setState({ selectScale: event.target.value }, () => {
      this.handleTempOnChangeScale(this.state.temperature);
    });
  };

  handleTempOnChangeScale = (tem) =>{
    var ttt = 0;
    if (this.state.selectScale === "C") {
      ttt = (parseFloat(tem) * 9) / 5 + 32;
      this.setState({ convertedTemperature: ttt + " F" });
    }else{
      ttt = (parseFloat(tem) - 32) * 5 / 9;
      this.setState({ convertedTemperature: ttt + " C" });
    }
  }

  handleChange(e) {
    this.setState({ temperature: e.target.value });
    this.handleTempOnChangeScale(e.target.value)  
  }

  render() {
    const temperature = this.state.temperature;
    return (
      <div>
        <fieldset>
          <legend>Enter temperature</legend>
          <input value={temperature} onChange={this.handleChange} />
          <select
            value={this.state.selectScale}
            onChange={this.handleSelectScale}
          >
            <option value="C">C</option>
            <option value="F">F</option>
          </select>
          <div>Temp is: {this.state.convertedTemperature}</div>
        </fieldset>
        <button onClick={this.props.returnMainPage}>Back to main Page</button>
      </div>
    );
  }
}
