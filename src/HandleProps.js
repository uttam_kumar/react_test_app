import React, { Component } from "react";

export default class HandleProps extends Component {

  state = {
    propsValue : ""
  }

  HandleAccessProps = () =>{
    this.setState({
      propsValue: this.props.propsFunc
    })
  }
  render() {
    console.log("render");
    return (
      <div>
        <button onClick={this.HandleAccessProps}>
          Prop Access
        </button><br/>
        <div>{this.state.propsValue}</div>
        <button onClick={this.props.returnMainPage}>
          Back to main Page
        </button>
      </div>
    );
  }
}
