import React, { Component } from "react";

export default class HandleLifeCycle extends Component {
  constructor(props) {
    super(props);
    console.log("constructor rom handle life cycle");
  }

  componentDidMount() {    
    console.log("componentDidMount from handle life cycle");
  }

  componentWillUnmount() {
    console.log("componentWillUnmount from handle life cycle");
  }

  render() {
    console.log("render from handle life cycle");
    return (
      <div>
          <div>see console for life cycle</div>
        <button onClick={this.props.returnMainPage}>
          Back to main Page
        </button>
      </div>
    );
  }
}
