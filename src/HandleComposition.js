import React, { Component } from "react";

export default class HandleComposition extends Component {
  constructor(props) {
    super(props);
    this.state = { login: "" };
  }

  handleChange = e => {
    this.setState({ login: e.target.value });
  };

  handleSignUp = () => {
    alert(`Welcome aboard, ${this.state.login}!`);
  };

  render() {
    function Dialog(props) {
      return (
        <FancyBorder color="#0BC3C6">
          <h1 className="Dialog-title">{props.title}</h1>
          <p className="Dialog-message">{props.message}</p>
        </FancyBorder>
      );
    }

    function WelcomeDialog() {
      return (
        <Dialog
          title="Welcome"
          message="Thank you for visiting our spacecraft!"
        />
      );
    }

    function FancyBorder(props) {
      return (
        <div style={{ backgroundColor: props.color }}>{props.children}</div>
      );
    }

    function SplitPane(props) {
      return (
        <div className="SplitPane">
          <div className="SplitPane-left">{props.left}</div>
          <div className="SplitPane-right">{props.right}</div>
        </div>
      );
    }

    function SecDialog(props) {
      return (
        <FancyBorder color="pink">
          <h1 className="Dialog-title">{props.title}</h1>
          <p className="Dialog-message">{props.message}</p>
          {props.children}
        </FancyBorder>
      );
    }

    return (
      <div>
        <div>
          <WelcomeDialog />
        </div>
        <div style={{ backgroundColor: "#6FE815" }}>
          <SplitPane left={"Contacts"} right={"Chat"} />
        </div>
        <div>
          <SecDialog
            title="Mars Exploration Program"
            message="How should we refer to you?"
          >
            <input
              type="text"
              value={this.state.login}
              onChange={this.handleChange}
            />
            <button onClick={this.handleSignUp}>Sign Me Up!</button>
          </SecDialog>
        </div>
        <button onClick={this.props.returnMainPage}>Back to main Page</button>

        <input
          type="text"
          value={this.state.login}
          onChange={this.handleChange}
        />
      </div>
    );
  }
}
